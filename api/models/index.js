var user      = require('./user');
var content      = require('./content');
var services      = require('./services');

module.exports = {
  user      : user,
  content      : content,
  services      : services
};