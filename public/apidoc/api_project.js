define({
  "name": "Xtras Apidoc",
  "version": "1.0.0",
  "description": "Xtras API documentation",
  "template": {
    "forceLanguage": "en"
  },
  "sampleUrl": false,
  "defaultVersion": "0.0.0",
  "apidoc": "0.3.0",
  "generator": {
    "name": "apidoc",
    "time": "2018-04-09T05:28:30.346Z",
    "url": "http://apidocjs.com",
    "version": "0.17.6"
  }
});
