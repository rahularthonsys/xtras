define({ "api": [
  {
    "type": "post",
    "url": "/get_coupon_by_service_id",
    "title": "Get Coupon by service_id",
    "group": "Coupon",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "service_id",
            "description": "<p>Service Id</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success",
          "content": "   HTTP/1.1 200 OK\n   {\n    \"status\": 200,\n    \"api_name\": \"get_coupon_by_service_id\",\n    \"message\": \"Coupon found.\",\n    \"data\": [\n        {\n            \"_id\": \"5ac6091ce10da115207753a2\",\n            \"coupon_code\": \"killer\",\n            \"expiry_date\": \"2018-04-08T16:00:00.000Z\",\n            \"created_at\": \"2018-04-05T11:31:40.089Z\",\n            \"multiple_use\": 1,\n            \"percent\": 50,\n            \"service_ids\": [\n                \"5ac46a660d4b66316d0ee73a\",\n                \"5ac47e52f3f11e3a458e74d7\",\n                \"5ac47e9af3f11e3a458e74db\",\n                \"5ac60678225aca1d6064420b\",\n                \"5ac6067d225aca1d6064420c\"\n            ],\n            \"is_deleted\": 0\n        },\n        {\n            \"_id\": \"5ac60edd255db317306f7047\",\n            \"coupon_code\": \"indian\",\n            \"expiry_date\": \"2018-04-08T16:00:00.000Z\",\n            \"created_at\": \"2018-04-05T11:56:13.614Z\",\n            \"multiple_use\": 1,\n            \"percent\": 50,\n            \"service_ids\": [\n                \"5ac46a660d4b66316d0ee73a\",\n                \"5ac47e52f3f11e3a458e74d7\",\n                \"5ac47e9af3f11e3a458e74db\",\n                \"5ac60678225aca1d6064420b\",\n                \"5ac6067d225aca1d6064420c\"\n            ],\n            \"is_deleted\": 0\n        },\n        {\n            \"_id\": \"5ac60ee2255db317306f7048\",\n            \"coupon_code\": \"must\",\n            \"expiry_date\": \"2018-04-08T16:00:00.000Z\",\n            \"created_at\": \"2018-04-05T11:56:18.806Z\",\n            \"multiple_use\": 1,\n            \"percent\": 50,\n            \"service_ids\": [\n                \"5ac46a660d4b66316d0ee73a\",\n                \"5ac47e52f3f11e3a458e74d7\",\n                \"5ac47e9af3f11e3a458e74db\",\n                \"5ac60678225aca1d6064420b\",\n                \"5ac6067d225aca1d6064420c\"\n            ],\n            \"is_deleted\": 0\n        }\n    ]\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Failed",
          "content": "HTTP/1.1 400 Failed\n   {\n      \"status\": 400,\n      \"api_name\": \"get_coupon_by_service_id\",\n      \"message\": \"Result not found.\",\n      \"data\": {}\n   }",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "api/controllers/user.js",
    "groupTitle": "Coupon",
    "name": "PostGet_coupon_by_service_id"
  },
  {
    "type": "post",
    "url": "/get_privacy_policy",
    "title": "Get privacy and policy",
    "group": "Frontend",
    "success": {
      "examples": [
        {
          "title": "Success",
          "content": "HTTP/1.1 200 OK\n{\n     \"status\": 200,\n     \"api_name\": \"get_privacy_policy\",\n     \"message\": \"Page found.\",\n     \"data\": {\n         \"_id\": \"5abe1dc758c77712747b3cec\",\n         \"key\": \"Privacy and Policy\",\n         \"value\": \"<p><strong>Lorem Ipsum</strong> is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\"\n     }\n     }",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Failed",
          "content": "HTTP/1.1 400 Failed\n   {\n       \"status\": 400,\n       \"api_name\": \"get_privacy_policy\",\n       \"message\": \"Page not found.\",\n       \"data\": {}\n   }",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "api/controllers/content.js",
    "groupTitle": "Frontend",
    "name": "PostGet_privacy_policy"
  },
  {
    "type": "post",
    "url": "/insert_contact_us",
    "title": "Contact us",
    "group": "Frontend",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "email",
            "description": "<p>Email</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "phone_no",
            "description": "<p>Phone Number</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "comment",
            "description": "<p>Comment</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success",
          "content": "HTTP/1.1 200 OK\n{\n       \"status\": 200,\n       \"api_name\": \"insert_contact_us\",\n       \"message\": \"Thank you for getting in touch!.\"\n   }",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Failed",
          "content": "HTTP/1.1 400 Failed\n   {\n       \"status\": 400,\n       \"api_name\": \"insert_contact_us\",\n       \"message\": \"Data not insert.\",\n       \"data\": {}\n   }",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "api/controllers/content.js",
    "groupTitle": "Frontend",
    "name": "PostInsert_contact_us"
  },
  {
    "type": "post",
    "url": "/get_category",
    "title": "Get Category",
    "group": "Home_Page",
    "success": {
      "examples": [
        {
          "title": "Success",
          "content": "   HTTP/1.1 200 OK\n   {\n\t\t\"status\": 200,\n\t\t\"api_name\": \"get_category\",\n\t\t\"message\": \"All Category.\",\n\t\t\"data\": [\n\t\t\t{\n\t\t\t\t\"_id\": \"5ac1e0a2d74b0f03981a5e6d\",\n\t\t\t\t\"category_name\": \"Automobile Services\",\n\t\t\t\t\"parent_id\": \"0\",\n\t\t\t\t\"category_image\": \"http://35.168.99.29:3001/image/automobile-svc.png\",\n\t\t\t\t\"created_at\": \"Mon Apr 30 2018 17:53:19 GMT+0530 (India Standard Time)\",\n\t\t\t\t\"updated_at\": \"Mon Apr 30 2018 17:53:19 GMT+0530 (India Standard Time)\",\n\t\t\t\t\"subcategories\": [\n\t\t\t\t\t{\n\t\t\t\t\t\t\"_id\": \"5ac4e5cd5f7b5e0f1101ec3d\",\n\t\t\t\t\t\t\"category_name\": \"Oil Change\",\n\t\t\t\t\t\t\"parent_id\": \"5ac1e0a2d74b0f03981a5e6d\",\n\t\t\t\t\t\t\"category_image\": \"http://35.168.99.29:3001/image/automobile-svc.png\",\n\t\t\t\t\t\t\"created_at\": \"Mon Apr 30 2018 17:53:19 GMT+0530 (India Standard Time)\",\n\t\t\t\t\t\t\"updated_at\": \"Mon Apr 30 2018 17:53:19 GMT+0530 (India Standard Time)\"\n\t\t\t\t\t},\n\t\t\t\t\t{\n\t\t\t\t\t\t\"_id\": \"5ac4e5df5f7b5e0f1101eca4\",\n\t\t\t\t\t\t\"category_name\": \"Tyre Rotation\",\n\t\t\t\t\t\t\"parent_id\": \"5ac1e0a2d74b0f03981a5e6d\",\n\t\t\t\t\t\t\"category_image\": \"http://35.168.99.29:3001/image/automobile-svc.png\",\n\t\t\t\t\t\t\"created_at\": \"Mon Apr 30 2018 17:53:19 GMT+0530 (India Standard Time)\",\n\t\t\t\t\t\t\"updated_at\": \"Mon Apr 30 2018 17:53:19 GMT+0530 (India Standard Time)\"\n\t\t\t\t\t},\n\t\t\t\t\t{\n\t\t\t\t\t\t\"_id\": \"5ac4e5f45f7b5e0f1101ed33\",\n\t\t\t\t\t\t\"category_name\": \"Wiper Blade\",\n\t\t\t\t\t\t\"parent_id\": \"5ac1e0a2d74b0f03981a5e6d\",\n\t\t\t\t\t\t\"category_image\": \"http://35.168.99.29:3001/image/automobile-svc.png\",\n\t\t\t\t\t\t\"created_at\": \"Mon Apr 30 2018 17:53:19 GMT+0530 (India Standard Time)\",\n\t\t\t\t\t\t\"updated_at\": \"Mon Apr 30 2018 17:53:19 GMT+0530 (India Standard Time)\"\n\t\t\t\t\t},\n\t\t\t\t\t{\n\t\t\t\t\t\t\"_id\": \"5ac4e6065f7b5e0f1101edb7\",\n\t\t\t\t\t\t\"category_name\": \"Battery Boost\",\n\t\t\t\t\t\t\"parent_id\": \"5ac1e0a2d74b0f03981a5e6d\",\n\t\t\t\t\t\t\"category_image\": \"http://35.168.99.29:3001/image/automobile-svc.png\",\n\t\t\t\t\t\t\"created_at\": \"Mon Apr 30 2018 17:53:19 GMT+0530 (India Standard Time)\",\n\t\t\t\t\t\t\"updated_at\": \"Mon Apr 30 2018 17:53:19 GMT+0530 (India Standard Time)\"\n\t\t\t\t\t},\n\t\t\t\t\t{\n\t\t\t\t\t\t\"_id\": \"5ac4e6155f7b5e0f1101ee1c\",\n\t\t\t\t\t\t\"category_name\": \"Others\",\n\t\t\t\t\t\t\"parent_id\": \"5ac1e0a2d74b0f03981a5e6d\",\n\t\t\t\t\t\t\"category_image\": \"http://35.168.99.29:3001/image/automobile-svc.png\",\n\t\t\t\t\t\t\"created_at\": \"Mon Apr 30 2018 17:53:19 GMT+0530 (India Standard Time)\",\n\t\t\t\t\t\t\"updated_at\": \"Mon Apr 30 2018 17:53:19 GMT+0530 (India Standard Time)\"\n\t\t\t\t\t}\n\t\t\t\t]\n\t\t\t},\n\t\t\t{\n\t\t\t\t\"_id\": \"5ac1e3f6d74b0f03981a5e6e\",\n\t\t\t\t\"category_name\": \"Computer Service\",\n\t\t\t\t\"parent_id\": \"0\",\n\t\t\t\t\"category_image\": \"http://35.168.99.29:3001/image/computer-svc.png\",\n\t\t\t\t\"created_at\": \"Mon Apr 30 2018 17:53:19 GMT+0530 (India Standard Time)\",\n\t\t\t\t\"updated_at\": \"Mon Apr 30 2018 17:53:19 GMT+0530 (India Standard Time)\",\n\t\t\t\t\"subcategories\": []\n\t\t\t},\n\t\t\t{\n\t\t\t\t\"_id\": \"5ac1e48fd74b0f03981a5e6f\",\n\t\t\t\t\"category_name\": \"Food Catering Service\",\n\t\t\t\t\"parent_id\": \"0\",\n\t\t\t\t\"category_image\": \"http://35.168.99.29:3001/image/food-svc.png\",\n\t\t\t\t\"created_at\": \"Mon Apr 30 2018 17:53:19 GMT+0530 (India Standard Time)\",\n\t\t\t\t\"updated_at\": \"Mon Apr 30 2018 17:53:19 GMT+0530 (India Standard Time)\",\n\t\t\t\t\"subcategories\": []\n\t\t\t},\n\t\t\t{\n\t\t\t\t\"_id\": \"5ac1e4fcd74b0f03981a5e70\",\n\t\t\t\t\"category_name\": \"Lawn Mowing Service\",\n\t\t\t\t\"parent_id\": \"0\",\n\t\t\t\t\"category_image\": \"http://35.168.99.29:3001/image/lawn-svc.png\",\n\t\t\t\t\"created_at\": \"Mon Apr 30 2018 17:53:19 GMT+0530 (India Standard Time)\",\n\t\t\t\t\"updated_at\": \"Mon Apr 30 2018 17:53:19 GMT+0530 (India Standard Time)\",\n\t\t\t\t\"subcategories\": []\n\t\t\t},\n\t\t\t{\n\t\t\t\t\"_id\": \"5ac1e55cd74b0f03981a5e71\",\n\t\t\t\t\"category_name\": \"Movers & Delivery Service\",\n\t\t\t\t\"parent_id\": \"0\",\n\t\t\t\t\"category_image\": \"http://35.168.99.29:3001/image/movers-svc.png\",\n\t\t\t\t\"created_at\": \"Mon Apr 30 2018 17:53:19 GMT+0530 (India Standard Time)\",\n\t\t\t\t\"updated_at\": \"Mon Apr 30 2018 17:53:19 GMT+0530 (India Standard Time)\",\n\t\t\t\t\"subcategories\": []\n\t\t\t},\n\t\t\t{\n\t\t\t\t\"_id\": \"5ac1e5a1d74b0f03981a5e72\",\n\t\t\t\t\"category_name\": \"Snow Cleaning Service\",\n\t\t\t\t\"parent_id\": \"0\",\n\t\t\t\t\"category_image\": \"http://35.168.99.29:3001/image/snow-svc.png\",\n\t\t\t\t\"created_at\": \"Mon Apr 30 2018 17:53:19 GMT+0530 (India Standard Time)\",\n\t\t\t\t\"updated_at\": \"Mon Apr 30 2018 17:53:19 GMT+0530 (India Standard Time)\",\n\t\t\t\t\"subcategories\": []\n\t\t\t}\n\t\t]\n\t}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Failed",
          "content": "HTTP/1.1 400 Failed\n   {\n       \"status\": 400,\n       \"api_name\": \"get_category\",\n       \"message\": \"No Category found.\"\n   }",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "api/controllers/user.js",
    "groupTitle": "Home_Page",
    "name": "PostGet_category"
  },
  {
    "type": "post",
    "url": "/add_appointments",
    "title": "Add appointments",
    "group": "Post",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "parent_appointment_id",
            "description": "<p>Parent appointment Id</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "consumer_id",
            "description": "<p>Cousumer ID</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "provider_id",
            "description": "<p>Provider ID</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "service_id",
            "description": "<p>Service Id</p>"
          },
          {
            "group": "Parameter",
            "type": "Array",
            "optional": false,
            "field": "service_addons",
            "description": "<p>Service Addons</p>"
          },
          {
            "group": "Parameter",
            "type": "Array",
            "optional": false,
            "field": "service_options",
            "description": "<p>Service options</p>"
          },
          {
            "group": "Parameter",
            "type": "Array",
            "optional": false,
            "field": "service_area_and_pricing",
            "description": "<p>Service Area And Pricing</p>"
          },
          {
            "group": "Parameter",
            "type": "Array",
            "optional": false,
            "field": "service_grass_snow_height",
            "description": "<p>Service Grass Snow Height</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "appointment_date",
            "description": "<p>Appointment Date</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "appointment_time",
            "description": "<p>Appointment Time</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "provider_firstname",
            "description": "<p>Provider Firstname</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "provider_lastname",
            "description": "<p>Provider Lastname</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "provider_company",
            "description": "<p>Provider Company</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "notes",
            "description": "<p>Notes</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "coupon_id",
            "description": "<p>ID Coupon Id</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success",
          "content": "   HTTP/1.1 200 OK\n   {\n\t\t\"status\": 200,\n\t\t\"api_name\": \"add_appointments\",\n\t\t\"message\": \"Appointment added successfully.\",\n\t\t\"data\": {\n\t\t\t\"parent_appointment_id\": \"1\",\n\t\t\t\"consumer_id\": \"5ac36f3bfa85e0150c53cf34\",\n\t\t\t\"provider_id\": \"5ac36f3bfa85e0150c53cf34\",\n            \"service_id\": \"3\",\n            \"service_addons\":[\"5ac36f3bfa8sdfsd0150c53cf34\",\"5ac36f3bfa8sdfsd0150c53cf34\"],\n            \"service_options\":[\"5ac36f3bfids9sdnsd50c53cf34\",\"5ac36f3bfids9sdnsd50c53cf34\"],\n            \"service_area_and_pricing\": \"\",\n            \"service_grass_snow_height\": \"\",\n\t\t\t\"appointment_date\": \"04-04-2018\",\n\t\t\t\"appointment_time\": \"09:50 PM\",\n\t\t\t\"provider_firstname\": \"Rahul\",\n\t\t\t\"provider_lastname\": \"soni\",\n\t\t\t\"provider_company\": \"sale news\",\n\t\t\t\"notes\": \"testing\",\n\t\t\t\"svc_option_ids\": \"\",\n\t\t\t\"svc_addon_ids\": \"\",\n\t\t\t\"coupon_id\": \"120245\",\n\t\t\t\"is_confirmed\": 1,\n\t\t\t\"created_at\": \"2018-04-04T07:47:04.763Z\",\n\t\t\t\"updated_at\": \"2018-04-04T07:47:04.763Z\",\n\t\t\t\"is_active\": 1,\n\t\t\t\"is_deleted\": 0,\n\t\t\t\"_id\": \"5ac482f80683cb0dc4f5d796\"\n\t\t}\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Failed",
          "content": "   HTTP/1.1 400 Failed\n      {\n\t\t\t\"status\": 400,\n\t\t\t\"api_name\": \"add_appointments\",\n\t\t\t\"message\": \"Some request parameters are missing.\",\n\t\t\t\"data\": {}\n\t  }",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "api/controllers/services.js",
    "groupTitle": "Post",
    "name": "PostAdd_appointments"
  },
  {
    "type": "post",
    "url": "/add_review",
    "title": "Add review for service",
    "group": "Post",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "user_id",
            "description": "<p>User Id</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "service_id",
            "description": "<p>Service Id</p>"
          },
          {
            "group": "Parameter",
            "type": "Integer",
            "optional": false,
            "field": "rate",
            "description": "<p>Rate</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "comment",
            "description": "<p>Comment</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success",
          "content": "   HTTP/1.1 200 OK\n   {\n    \"status\": 200,\n    \"api_name\": \"add_review\",\n    \"message\": \"Review add successfully.\",\n    \"data\": {\n        \"service_id\": \"5ac46a660d4b66316d0ee73a\",\n        \"user_id\": \"5ac32682bc4d0f29ad7a7a7f\",\n        \"rate\": \"5\",\n        \"comment\": \"good service\",\n        \"created_at\": \"2018-04-05T14:35:38.125Z\",\n        \"updated_at\": \"2018-04-05T14:35:38.125Z\",\n        \"_id\": \"5ac6343aaeb11a1494adbe2e\"\n    }\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Failed",
          "content": "HTTP/1.1 400 Failed\n  {\n     \"status\": 400,\n     \"api_name\": \"add_review\",\n     \"message\": \"Review already exist.\",\n     \"data\": {}\n }",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "api/controllers/services.js",
    "groupTitle": "Post",
    "name": "PostAdd_review"
  },
  {
    "type": "post",
    "url": "/get_appointments",
    "title": "Get appointments",
    "group": "Post",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "user_id",
            "description": "<p>User Id</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success",
          "content": "   HTTP/1.1 200 OK\n   {\n    \"status\": 400,\n    \"api_name\": \"get_appointments\",\n    \"message\": \"All appointments found.\",\n    \"data\": [\n        {\n            \"_id\": \"5ac48215c4af2b14a047ad5d\",\n            \"parent_appointment_id\": \"1\",\n            \"consumer_id\": \"5ac36f3bfa85e0150c53cf34\",\n            \"provider_id\": \"5ac36f3bfa85e0150c53cf34\",\n            \"service_id\": \"3\",\n            \"appointment_date\": \"04-04-2018\",\n            \"appointment_time\": \"09:50 PM\",\n            \"provider_firstname\": \"Rahul\",\n            \"provider_lastname\": \"soni\",\n            \"provider_company\": \"sale news\",\n            \"notes\": \"testing\",\n            \"svc_option_ids\": \"\",\n            \"svc_addon_ids\": \"\",\n            \"coupon_id\": \"120245\",\n            \"is_confirmed\": 1,\n            \"created_at\": \"2018-04-04T07:43:17.700Z\",\n            \"updated_at\": \"2018-04-04T07:43:17.700Z\",\n            \"is_active\": 1,\n            \"is_deleted\": 0\n        },\n        {\n            \"_id\": \"5ac4afa639118d061ce596ad\",\n            \"parent_appointment_id\": \"1\",\n            \"consumer_id\": \"5ac36f3bfa85e0150c53cf34\",\n            \"provider_id\": \"5ac36f3bfa85e0150c53cf34\",\n            \"service_id\": \"3\",\n            \"appointment_date\": \"04-04-2018\",\n            \"appointment_time\": \"09:50 PM\",\n            \"provider_firstname\": \"Rahul\",\n            \"provider_lastname\": \"soni\",\n            \"provider_company\": \"sale news\",\n            \"notes\": \"testing\",\n            \"svc_option_ids\": \"\",\n            \"svc_addon_ids\": \"\",\n            \"coupon_id\": \"120245\",\n            \"is_confirmed\": 1,\n            \"created_at\": \"2018-04-04T07:43:17.700Z\",\n            \"updated_at\": \"2018-04-04T07:43:17.700Z\",\n            \"is_active\": 1,\n            \"is_deleted\": 0\n        }\n    ]\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Failed",
          "content": "   HTTP/1.1 400 Failed\n      {\n\t\t\t\"status\": 400,\n\t\t\t\"api_name\": \"get_appointments\",\n\t\t\t\"message\": \"Some request parameters are missing.\",\n\t\t\t\"data\": {}\n\t  }",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "api/controllers/services.js",
    "groupTitle": "Post",
    "name": "PostGet_appointments"
  },
  {
    "type": "post",
    "url": "/get_posts",
    "title": "Get Posts",
    "group": "Post",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "service_category_id",
            "description": "<p>ID of the Service Category for which posts need to be fetched</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "type",
            "description": "<p>Service Type(individual/ business), or, Price (price)</p>"
          },
          {
            "group": "Parameter",
            "type": "Integer",
            "optional": true,
            "field": "limit",
            "description": "<p>Number of records to be fetched at a time (e.g 20)</p>"
          },
          {
            "group": "Parameter",
            "type": "Integer",
            "optional": true,
            "field": "page",
            "description": "<p>Page number (e.g 1)</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success",
          "content": "   {\n\t\t\"status\": 200,\n\t\t\"api_name\": \"get_posts\",\n\t\t\"message\": \"Posts records retrieved successfully\",\n\t\t\"data\": [\n\t\t\t{\n\t\t\t\t\"_id\": \"5ac70a6144b1f41aa0ee455d\",\n\t\t\t\t\"service_category_id\": \"5ac1e4fcd74b0f03981a5e70\",\n\t\t\t\t\"service_name\": \"Lawn Mowing Service\",\n\t\t\t\t\"service_type\": \"individual\",\n\t\t\t\t\"additional_details\": \"\",\n\t\t\t\t\"service_radius\": \"552\",\n\t\t\t\t\"service_radius_units\": \"Km\",\n\t\t\t\t\"weekday_start_time\": \"\",\n\t\t\t\t\"weekday_stop_time\": \"\",\n\t\t\t\t\"weekend_start_time\": \"\",\n\t\t\t\t\"weekend_stop_time\": \"\",\n\t\t\t\t\"available_monday\": \"1\",\n\t\t\t\t\"available_tuesday\": \"1\",\n\t\t\t\t\"available_wednesday\": \"1\",\n\t\t\t\t\"available_thursday\": \"1\",\n\t\t\t\t\"available_friday\": \"1\",\n\t\t\t\t\"available_saturday\": \"0\",\n\t\t\t\t\"available_sunday\": \"0\",\n\t\t\t\t\"is_active\": \"0\",\n\t\t\t\t\"cancel_hours\": \"24\",\n\t\t\t\t\"cancel_fee\": \"50\",\n\t\t\t\t\"reschedule_hours\": \"24\",\n\t\t\t\t\"reschedule_fee\": \"50\",\n\t\t\t\t\"cancel_rsh_policy\": \"\",\n\t\t\t\t\"legal_policy\": \"\",\n\t\t\t\t\"address\": \"21/30 Kaveri Path\",\n\t\t\t\t\"city\": \"Jaipur\",\n\t\t\t\t\"province\": \"Rajasthan\",\n\t\t\t\t\"zipcode\": \"302017\",\n\t\t\t\t\"country\": \"India\",\n\t\t\t\t\"rating\": \"0\",\n\t\t\t\t\"userdata\": {\n\t\t\t\t\t\"_id\": \"5ac2235feab4d71710a0521c\",\n\t\t\t\t\t\"fullname\": \"Mike Adams\",\n\t\t\t\t\t\"user_role\": 2,\n\t\t\t\t\t\"email\": \"mike.adams@mailinator.com\",\n\t\t\t\t\t\"alternate_email\": \"\",\n\t\t\t\t\t\"phone\": \"\",\n\t\t\t\t\t\"phone_1\": \"\",\n\t\t\t\t\t\"phone_2\": \"\",\n\t\t\t\t\t\"address\": \"\",\n\t\t\t\t\t\"address_1\": \"\",\n\t\t\t\t\t\"address_2\": \"\",\n\t\t\t\t\t\"city\": \"\",\n\t\t\t\t\t\"state\": \"\",\n\t\t\t\t\t\"zip_code\": \"\",\n\t\t\t\t\t\"country\": \"\",\n\t\t\t\t\t\"latitude\": \"\",\n\t\t\t\t\t\"longitude\": \"\",\n\t\t\t\t\t\"password\": \"333f44ba2976b0\",\n\t\t\t\t\t\"user_image\": \"http://35.168.99.29:3001/image/automobile-svc.png\",\n\t\t\t\t\t\"facebook_login_id\": \"348574680756857680\",\n\t\t\t\t\t\"google_login_id\": \"\",\n\t\t\t\t\t\"social_login_data_status\": 1,\n\t\t\t\t\t\"otp_status\": 0,\n\t\t\t\t\t\"is_active\": 0,\n\t\t\t\t\t\"is_deleted\": 0,\n\t\t\t\t\t\"created_time\": \"2018-04-02T12:34:39.500Z\",\n\t\t\t\t\t\"modified_time\": \"2018-04-02T12:34:39.501Z\",\n\t\t\t\t\t\"profile_complete\": 0\n\t\t\t\t},\n\t\t\t\t\"service_area_and_pricing\": [\n\t\t\t\t\t{\n\t\t\t\t\t\t\"_id\": \"5ac70a6144b1f41aa0ee4558\",\n\t\t\t\t\t\t\"area_from_sqft\": \"11\",\n\t\t\t\t\t\t\"area_to_sqft\": \"11\",\n\t\t\t\t\t\t\"price\": \"11\"\n\t\t\t\t\t}\n\t\t\t\t],\n\t\t\t\t\"service_grass_snow_height\": [\n\t\t\t\t\t{\n\t\t\t\t\t\t\"_id\": \"5ac70a6144b1f41aa0ee4559\",\n\t\t\t\t\t\t\"area_from_sqft\": \"11\",\n\t\t\t\t\t\t\"area_to_sqft\": \"11\",\n\t\t\t\t\t\t\"price\": \"11\"\n\t\t\t\t\t}\n\t\t\t\t],\n\t\t\t\t\"service_addons\": \"\",\n\t\t\t\t\"service_options\": [\n\t\t\t\t\t{\n\t\t\t\t\t\t\"_id\": \"5ac70a6144b1f41aa0ee455a\",\n\t\t\t\t\t\t\"name\": \"sdd\",\n\t\t\t\t\t\t\"price\": \"21\"\n\t\t\t\t\t}\n\t\t\t\t],\n\t\t\t\t\"service_uploads\": [\n\t\t\t\t\t\"http://127.0.0.1:3001/uploads/services/service_1522993761935488.jpg\",\n\t\t\t\t\t\"http://127.0.0.1:3001/uploads/services/service_1522993761940246.jpg\"\n\t\t\t\t]\n\t\t\t}\n\t\t]\n\t}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Failed",
          "content": "HTTP/1.1 400 Failed\n   {\n       \"status\": 400,\n       \"api_name\": \"get_posts\",\n       \"message\": \"Invalid request parameters.\",\n       \"data\": {}\n   }",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "api/controllers/services.js",
    "groupTitle": "Post",
    "name": "PostGet_posts"
  },
  {
    "type": "post",
    "url": "/post_service",
    "title": "Post Service",
    "group": "Post",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "user_id",
            "description": "<p>ID of the User submitting the post</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "service_category_id",
            "description": "<p>ID of the referred category</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "service_name",
            "description": "<p>Service name</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "service_type",
            "description": "<p>Service Type(individual/ business)</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "additional_details",
            "description": "<p>Additional Details</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "service_radius",
            "description": "<p>Service Radius</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "service_radius_units",
            "description": "<p>Service Radius Units (Miles/Km)</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "weekday_start_time",
            "description": "<p>weekday_start_time(10:00)</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "weekday_stop_time",
            "description": "<p>weekday_stop_time(18:00)</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "weekend_start_time",
            "description": "<p>weekend_start_time(10:00)</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "weekend_stop_time",
            "description": "<p>weekend_stop_time(17:00)</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "available_monday",
            "description": "<p>available_monday(1/0)</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "available_tuesday",
            "description": "<p>available_tuesday(1/0)</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "available_wednesday",
            "description": "<p>available_wednesday(1/0)</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "available_thursday",
            "description": "<p>available_thursday(1/0)</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "available_friday",
            "description": "<p>available_friday(1/0)</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "available_saturday",
            "description": "<p>available_saturday(1/0)</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "available_sunday",
            "description": "<p>available_sunday(1/0)</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "cancel_hours",
            "description": "<p>cancel_hours(24)</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "cancel_fee",
            "description": "<p>cancel_fee(50)</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "reschedule_hours",
            "description": "<p>reschedule_hours(24)</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "reschedule_fee",
            "description": "<p>reschedule_fee(40)</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "cancel_rsh_policy",
            "description": "<p>Cancel/reschedule Policy</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "legal_policy",
            "description": "<p>Legal Policy</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "address",
            "description": "<p>Address</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "city",
            "description": "<p>City</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "province",
            "description": "<p>Province</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "zipcode",
            "description": "<p>Zipcode</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "country",
            "description": "<p>Country</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "service_area",
            "description": "<p>Service Area Details (&quot;{ &quot;area&quot;: [ { &quot;area_from_sqft&quot;: &quot;11&quot; , &quot;area_to_sqft&quot;: &quot;11&quot;,  &quot;price&quot;: &quot;11&quot; } ] }&quot;)</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "grass_snow_height",
            "description": "<p>Grass/Snow Height Details (&quot;{ &quot;grasssnowheight&quot;: [ { &quot;area_from_sqft&quot;: &quot;11&quot;,      &quot;area_to_sqft&quot;: &quot;11&quot;,      &quot;price&quot;: &quot;11&quot; } ] }&quot;)</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "service_adons",
            "description": "<p>Service Addon Details (&quot;{ &quot;addon&quot;: [ { &quot;name&quot;: &quot;abc&quot;,    &quot;price&quot;: &quot;11&quot; } ] }&quot;)</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "service_options",
            "description": "<p>Service Options Details (&quot;{ &quot;option&quot;: [ { &quot;name&quot;: &quot;abc&quot;,    &quot;price&quot;: &quot;11&quot; } ] }&quot;)</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "uploads",
            "description": "<p>Images in JS array</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success",
          "content": "   {\n\t\t\"status\": 200,\n\t\t\"api_name\": \"post_service\",\n\t\t\"message\": \"You have posted Service successfully.\",\n\t\t\"data\": {\n\t\t\t\"token\": \"eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHBpcmVzIjoxNTIzMDgwMTYxOTQzfQ.313bmWMZnm2blSOK1TqN9xGbw9JOqO8TxJhvCdQBQwk\",\n\t\t\t\"expires\": 1523080161943,\n\t\t\t\"services\": {\n\t\t\t\t\"service_category_id\": \"5ac1e4fcd74b0f03981a5e70\",\n\t\t\t\t\"service_name\": \"Lawn Mowing Service\",\n\t\t\t\t\"service_type\": \"individual\",\n\t\t\t\t\"additional_details\": \"\",\n\t\t\t\t\"service_radius\": \"552\",\n\t\t\t\t\"service_radius_units\": \"Km\",\n\t\t\t\t\"weekday_start_time\": \"\",\n\t\t\t\t\"weekday_stop_time\": \"\",\n\t\t\t\t\"weekend_start_time\": \"\",\n\t\t\t\t\"weekend_stop_time\": \"\",\n\t\t\t\t\"available_monday\": \"1\",\n\t\t\t\t\"available_tuesday\": \"1\",\n\t\t\t\t\"available_wednesday\": \"1\",\n\t\t\t\t\"available_thursday\": \"1\",\n\t\t\t\t\"available_friday\": \"1\",\n\t\t\t\t\"available_saturday\": \"0\",\n\t\t\t\t\"available_sunday\": \"0\",\n\t\t\t\t\"is_active\": \"0\",\n\t\t\t\t\"cancel_hours\": \"24\",\n\t\t\t\t\"cancel_fee\": \"50\",\n\t\t\t\t\"reschedule_hours\": \"24\",\n\t\t\t\t\"reschedule_fee\": \"50\",\n\t\t\t\t\"cancel_rsh_policy\": \"\",\n\t\t\t\t\"legal_policy\": \"\",\n\t\t\t\t\"address\": \"21/30 Kaveri Path\",\n\t\t\t\t\"city\": \"Jaipur\",\n\t\t\t\t\"province\": \"Rajasthan\",\n\t\t\t\t\"zipcode\": \"302017\",\n\t\t\t\t\"country\": \"India\",\n\t\t\t\t\"rating\": \"0\",\n\t\t\t\t\"userdata\": {\n\t\t\t\t\t\"_id\": \"5ac2235feab4d71710a0521c\",\n\t\t\t\t\t\"fullname\": \"Mike Adams\",\n\t\t\t\t\t\"user_role\": 2,\n\t\t\t\t\t\"email\": \"mike.adams@mailinator.com\",\n\t\t\t\t\t\"alternate_email\": \"\",\n\t\t\t\t\t\"phone\": \"\",\n\t\t\t\t\t\"phone_1\": \"\",\n\t\t\t\t\t\"phone_2\": \"\",\n\t\t\t\t\t\"address\": \"\",\n\t\t\t\t\t\"address_1\": \"\",\n\t\t\t\t\t\"address_2\": \"\",\n\t\t\t\t\t\"city\": \"\",\n\t\t\t\t\t\"state\": \"\",\n\t\t\t\t\t\"zip_code\": \"\",\n\t\t\t\t\t\"country\": \"\",\n\t\t\t\t\t\"latitude\": \"\",\n\t\t\t\t\t\"longitude\": \"\",\n\t\t\t\t\t\"password\": \"333f44ba2976b0\",\n\t\t\t\t\t\"user_image\": \"http://35.168.99.29:3001/image/automobile-svc.png\",\n\t\t\t\t\t\"facebook_login_id\": \"348574680756857680\",\n\t\t\t\t\t\"google_login_id\": \"\",\n\t\t\t\t\t\"social_login_data_status\": 1,\n\t\t\t\t\t\"otp_status\": 0,\n\t\t\t\t\t\"is_active\": 0,\n\t\t\t\t\t\"is_deleted\": 0,\n\t\t\t\t\t\"created_time\": \"2018-04-02T12:34:39.500Z\",\n\t\t\t\t\t\"modified_time\": \"2018-04-02T12:34:39.501Z\",\n\t\t\t\t\t\"profile_complete\": 0\n\t\t\t\t},\n\t\t\t\t\"service_area_and_pricing\": [\n\t\t\t\t\t{\n\t\t\t\t\t\t\"_id\": \"5ac70a6144b1f41aa0ee4558\",\n\t\t\t\t\t\t\"area_from_sqft\": \"11\",\n\t\t\t\t\t\t\"area_to_sqft\": \"11\",\n\t\t\t\t\t\t\"price\": \"11\"\n\t\t\t\t\t}\n\t\t\t\t],\n\t\t\t\t\"service_grass_snow_height\": [\n\t\t\t\t\t{\n\t\t\t\t\t\t\"_id\": \"5ac70a6144b1f41aa0ee4559\",\n\t\t\t\t\t\t\"area_from_sqft\": \"11\",\n\t\t\t\t\t\t\"area_to_sqft\": \"11\",\n\t\t\t\t\t\t\"price\": \"11\"\n\t\t\t\t\t}\n\t\t\t\t],\n\t\t\t\t\"service_addons\": \"\",\n\t\t\t\t\"service_options\": [\n\t\t\t\t\t{\n\t\t\t\t\t\t\"_id\": \"5ac70a6144b1f41aa0ee455a\",\n\t\t\t\t\t\t\"name\": \"sdd\",\n\t\t\t\t\t\t\"price\": \"21\"\n\t\t\t\t\t}\n\t\t\t\t],\n\t\t\t\t\"service_uploads\": [\n\t\t\t\t\t\"http://127.0.0.1:3001/uploads/services/service_1522993761935488.jpg\",\n\t\t\t\t\t\"http://127.0.0.1:3001/uploads/services/service_1522993761940246.jpg\"\n\t\t\t\t]\n\t\t\t}\n\t\t}\n\t}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Failed",
          "content": "HTTP/1.1 400 Failed\n   {\n       \"status\": 400,\n       \"api_name\": \"post_service\",\n       \"message\": \"Invalid request parameters.\",\n       \"data\": {}\n   }",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "api/controllers/services.js",
    "groupTitle": "Post",
    "name": "PostPost_service"
  },
  {
    "type": "post",
    "url": "/search_by_keyword",
    "title": "Post Service",
    "group": "Search",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "search_keyword",
            "description": "<p>Keyword for search</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success",
          "content": "   {\n    \"status\": 200,\n    \"api_name\": \"search_by_keyword\",\n    \"message\": \"You have Search Service successfully.\",\n    \"data\": {\n        \"token\": \"eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHBpcmVzIjoxNTIyODUyOTQzNjI0LCJzZXJ2aWNlc0lEIjoiNWFjMzkyY2YzODY4OGMxMjFjMDM2YmFiIn0.7jfEHnUfj4Df5-F5KduUztM4AK3_pn4_F-cfMD6K5qM\",\n        \"expires\": 1522852943624,\n        \"services\": [ {\n            \"_id\": \"5ac4699e0d4b66316d0ee736\",\n            \"service_category_id\": \"ObjectId('5ac1e4fcd74b0f03981a5e70')\",\n            \"service_name\": \"Lawn Cleaning\",\n            \"service_type\": \"individual\",\n            \"additional_details\": \"dtls...\",\n            \"service_radius\": \"5.0\",\n            \"service_radius_units\": \"Km\",\n            \"weekday_start_time\": \"10:00\",\n            \"weekday_stop_time\": \"18:00\",\n            \"weekend_start_time\": \"10.00\",\n            \"weekend_stop_time\": \"17:00\",\n            \"available_monday\": \"1\",\n            \"available_tuesday\": \"1\",\n            \"available_wednesday\": \"1\",\n            \"available_thursday\": \"1\",\n            \"available_friday\": \"1\",\n            \"available_saturday\": \"0\",\n            \"available_sunday\": \"0\",\n            \"is_active\": \"0\",\n            \"cancel_hours\": \"24\",\n            \"cancel_fee\": \"50\",\n            \"reschedule_hours\": \"24\",\n            \"reschedule_fee\": \"50\",\n            \"cancel_rsh_policy\": \"\",\n            \"legal_policy\": \"\",\n            \"address\": \"21/30 Kaveri Path\",\n            \"city\": \"Jaipur\",\n            \"province\": \"Rajasthan\",\n            \"zipcode\": \"302017\",\n            \"country\": \"India\",\n            \"rating\": \"0\",\n            \"image\": \"http://35.168.99.29:3001/uploads/services/service_1522821734344139.jpg\"\n        }]\n    }\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Failed",
          "content": "HTTP/1.1 400 Failed\n   {\n       \"status\": 400,\n       \"api_name\": \"search_by_keyword\",\n       \"message\": \"Invalid request parameters.\",\n       \"data\": {}\n   }",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "api/controllers/search.js",
    "groupTitle": "Search",
    "name": "PostSearch_by_keyword"
  },
  {
    "type": "post",
    "url": "/add_faq",
    "title": "Add user query",
    "group": "User",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "email",
            "description": "<p>Email ID of User</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "query",
            "description": "<p>Query by user</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success",
          "content": "   HTTP/1.1 200 OK\n   {\n    \"status\": 200,\n    \"api_name\": \"add_faq\",\n    \"message\": \"Query Added successfully.\",\n    \"data\": {}\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Failed",
          "content": "HTTP/1.1 400 Failed\n   {\n      \"status\": 400,\n      \"api_name\": \"add_faq\",\n      \"message\": \"Query not Added.\",\n      \"data\": {}\n   }",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "api/controllers/user.js",
    "groupTitle": "User",
    "name": "PostAdd_faq"
  },
  {
    "type": "post",
    "url": "/change_password",
    "title": "Change Password",
    "group": "User",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "user_id",
            "description": "<p>User Id</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "old_password",
            "description": "<p>Old Password</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "new_password",
            "description": "<p>New Password</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success",
          "content": "HTTP/1.1 200 OK\n{\n       \"status\": 200,\n       \"api_name\": \"change_password\",\n       \"message\": \"You have changed password successfully.\",\n       \"data\": 1\n   }",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Failed",
          "content": "HTTP/1.1 400 Failed\n   {\n       \"status\": 400,\n       \"api_name\": \"change_password\",\n       \"message\": \"Old password is wrong.\",\n       \"data\": {}\n   }",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "api/controllers/user.js",
    "groupTitle": "User",
    "name": "PostChange_password"
  },
  {
    "type": "post",
    "url": "/resend_otp",
    "title": "Resend OTP",
    "group": "User",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "email",
            "description": "<p>Email</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success",
          "content": "HTTP/1.1 200 OK\n{\n       \"status\": 200,\n       \"api_name\": \"resend_otp\",\n       \"message\": \"OTP has been sent to your email address.\",\n       \"data\": {\n           \"user_id\": \"5ab9f851dd88353ccc65cbf2\",\n           \"user_email\": \"rajeev@gmail.com\",\n           \"otp_number\": \"23105\",\n           \"_id\": \"5aba5155a0dc7f0585a1fb98\"\n       }\n   }",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Failed",
          "content": "HTTP/1.1 400 Failed\n   {\n       \"status\": 400,\n       \"api_name\": \"resend_otp\",\n       \"message\": \"User email is not exist.\",\n       \"data\": {}\n   }",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "api/controllers/user.js",
    "groupTitle": "User",
    "name": "PostResend_otp"
  },
  {
    "type": "post",
    "url": "/social_login",
    "title": "Social Login",
    "group": "User",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "email",
            "description": "<p>Email ID of User</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "login_id",
            "description": "<p>Facebook/Google ID</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "login_type",
            "description": "<p>&quot;facebook&quot; or &quot;google&quot;</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "image_url",
            "description": "<p>URL of profile image by Google/Facebook</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "device_token",
            "description": "<p>Device Token</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "device_type",
            "description": "<p>Device Type : android / ios</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "name",
            "description": "<p>Full Name of User</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success",
          "content": "   HTTP/1.1 200 OK\n   {\n\t\t\"status\": 200,\n\t\t\"api_name\": \"social_login\",\n\t\t\"message\": \"Facebook Login is successful\",\n\t\t\"data\": {\n\t\t\t\"token\": \"eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHBpcmVzIjoxNTIyNzU4ODc5NTMxLCJ1c2VySUQiOiI1YWMyMjM1ZmVhYjRkNzE3MTBhMDUyMWMifQ.n2B7E5pWwG0tu3F82bQ7LBkqB74YtGrdydItEhqgXIc\",\n\t\t\t\"expires\": 1522758879531,\n\t\t\t\"user\": {\n\t\t\t\t\"fullname\": \"Mike Adams\",\n\t\t\t\t\"user_role\": 2,\n\t\t\t\t\"email\": \"mike.adams@mailinator.com\",\n\t\t\t\t\"alternate_email\": \"\",\n\t\t\t\t\"phone\": \"\",\n\t\t\t\t\"phone_1\": \"\",\n\t\t\t\t\"phone_2\": \"\",\n\t\t\t\t\"address\": \"\",\n\t\t\t\t\"address_1\": \"\",\n\t\t\t\t\"address_2\": \"\",\n\t\t\t\t\"city\": \"\",\n\t\t\t\t\"state\": \"\",\n\t\t\t\t\"zip_code\": \"\",\n\t\t\t\t\"country\": \"\",\n\t\t\t\t\"latitude\": \"\",\n\t\t\t\t\"longitude\": \"\",\n\t\t\t\t\"password\": \"333f44ba2976b0\",\n\t\t\t\t\"user_image\": \"http://35.168.99.29:3001/image/automobile-svc.png\",\n\t\t\t\t\"facebook_login_id\": \"348574680756857680\",\n\t\t\t\t\"google_login_id\": \"\",\n\t\t\t\t\"social_login_data_status\": 1,\n\t\t\t\t\"otp_status\": 0,\n\t\t\t\t\"is_active\": 0,\n\t\t\t\t\"is_deleted\": 0,\n\t\t\t\t\"created_time\": \"2018-04-02T12:34:39.500Z\",\n\t\t\t\t\"modified_time\": \"2018-04-02T12:34:39.501Z\",\n\t\t\t\t\"_id\": \"5ac2235feab4d71710a0521c\"\n\t\t\t}\n\t\t}\n\t}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Failed",
          "content": "HTTP/1.1 400 Failed\n   {\n       \"status\": 400,\n       \"api_name\": \"social_login\",\n       \"message\": \"Email already exists\",\n       \"data\": {}\n   }",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "api/controllers/user.js",
    "groupTitle": "User",
    "name": "PostSocial_login"
  },
  {
    "type": "post",
    "url": "/update_forgot_password",
    "title": "Update Forgot Password",
    "group": "User",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "user_id",
            "description": "<p>User Id</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "new_password",
            "description": "<p>New Password</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success",
          "content": "HTTP/1.1 200 OK\n{\n       \"status\": 200,\n       \"api_name\": \"update_forgot_password\",\n       \"message\": \"You have changed password successfully.\",\n       \"data\": {}\n   }",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Failed",
          "content": "HTTP/1.1 400 Failed\n   {\n       \"status\": 400,\n       \"api_name\": \"update_forgot_password\",\n       \"message\": \"User doesn't exist.\",\n       \"data\": {}\n   }",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "api/controllers/user.js",
    "groupTitle": "User",
    "name": "PostUpdate_forgot_password"
  },
  {
    "type": "post",
    "url": "/update_profile",
    "title": "Update Profile",
    "group": "User",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "fullname",
            "description": "<p>Full name</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "alternate_email",
            "description": "<p>Alternate_email</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "email",
            "description": "<p>Email</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "phone",
            "description": "<p>Phone</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "phone_1",
            "description": "<p>Phone 1</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "phone_2",
            "description": "<p>Phone 2</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "address",
            "description": "<p>Address</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "address_1",
            "description": "<p>Address_1</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "address_2",
            "description": "<p>Address_2</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "user_id",
            "description": "<p>User id</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "user_image",
            "description": "<p>User_image</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "city",
            "description": "<p>city</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "state",
            "description": "<p>state</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "country",
            "description": "<p>country</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "zip_code",
            "description": "<p>zip_code</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success",
          "content": "   HTTP/1.1 200 OK\n   {\n\t\t\t\"status\": 200,\n\t\t\t\"api_name\": \"update_profile\",\n\t\t\t\"message\": \"You have updated successfully\",\n\t\t\t\"data\": {\n\t\t\t\t\"token\": \"eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHBpcmVzIjoxNTIyNDc2NjA3ODY2LCJ1c2VySUQiOiI1YWJkZDQyNjAyYWNlYTM2Mjk3OGVkODcifQ.RLWoRl_qqzSfTBWsywB7yYaGjwuoGtTrFBmEM2Ifo3M\",\n\t\t\t\t\"expires\": 1522476607866,\n\t\t\t\t\"user\": {\n\t\t\t\t\t\"user_role\": 2,\n\t\t\t\t\t\"alternate_email\": \"rohit@gmail.com\",\n\t\t\t\t\t\"email\": \"rohit@gmail.com\",\n\t\t\t\t\t\"phone\": \"7877949375\",\n\t\t\t\t\t\"phone_1\": \"7877949375\",\n\t\t\t\t\t\"phone_2\": \"7877949375\",\n\t\t\t\t\t\"address\": \"jothwara,jaipur\",\n\t\t\t\t\t\"address_1\": \"jaipur jaipur\",\n\t\t\t\t\t\"address_2\": \"hariyan\",\n\t\t\t\t\t\"city\": \"jaipur\",\n\t\t\t\t\t\"state\": \"rajasthan\",\n\t\t\t\t\t\"zip_code\": \"302012\",\n\t\t\t\t\t\"country\": \"india\",\n\t\t\t\t\t\"latitude\": 26.9564325,\n\t\t\t\t\t\"longitude\": 75.74125339999999,\n\t\t\t\t\t\"facebook_login_id\": \"\",\n\t\t\t\t\t\"google_login_id\": \"\",\n\t\t\t\t\t\"social_login_data_status\": 0,\n\t\t\t\t\t\"otp_status\": 0,\n\t\t\t\t\t\"is_active\": 0,\n\t\t\t\t\t\"is_deleted\": 0,\n\t\t\t\t\t\"created_time\": \"2018-03-30T06:10:07.852Z\",\n\t\t\t\t\t\"modified_time\": \"2018-03-30T06:10:07.852Z\",\n\t\t\t\t\t\"user_image\": \"users_1522390207852498.jpg\",\n\t\t\t\t\t\"_id\": \"5abdd42602acea362978ed87\"\n\t\t\t\t}\n\t\t\t}\n\t\t}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Failed",
          "content": "HTTP/1.1 400 Failed\n       {\n           \"status\": 400,\n           \"api_name\": \"update_profile\",\n           \"message\": \"Phone is already exist.\",\n           \"data\": {}\n       }",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "api/controllers/user.js",
    "groupTitle": "User",
    "name": "PostUpdate_profile"
  },
  {
    "type": "post",
    "url": "/user_login",
    "title": "User Login",
    "group": "User",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "email",
            "description": "<p>Email</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "password",
            "description": "<p>Password</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "device_token",
            "description": "<p>Device Token</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "device_type",
            "description": "<p>Device Type : android / ios</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success",
          "content": "   {\n    \"status\": 200,\n    \"api_name\": \"user_login\",\n    \"message\": \"You have login successfully.\",\n    \"data\": {\n        \"token\": \"eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHBpcmVzIjoxNTIyMzg5MzM0NzA4LCJ1c2VySUQiOiI1YWJjN2Q4YzZlYmQ0MzEyZjRiM2QyMjkifQ.FDJQCRJxwKLe_k5aOroIWmeR7j7hrKCKcL8CSbfJPsc\",\n        \"expires\": 1522389334708,\n        \"user\": {\n            \"_id\": \"5abc7d8c6ebd4312f4b3d229\",\n            \"fullname\": \"rahul soni\",\n            \"user_role\": 2,\n            \"email\": \"rahul@gmail.com\",\n            \"alternate_email\": \"\",\n            \"phone\": \"\",\n\t\t\t\"phone_1\": \"\",\n\t\t\t\"phone_2\": \"\",\n            \"address\": \"jothwara,jaipur\",\n            \"address_1\": \"\",\n            \"address_2\": \"\",\n            \"city\": \"jaipur\",\n            \"state\": \"rajasthan\",\n            \"zip_code\": \"302012\",\n            \"country\": \"india\",\n            \"latitude\": 26.9564325,\n            \"longitude\": 75.74125339999999,\n            \"password\": \"777e11fa2c71\",\n            \"user_image\": \"http://localhost:3001/uploads/default/default_user.jpg\",\n            \"facebook_login_id\": \"\",\n            \"google_login_id\": \"\",\n            \"social_login_data_status\": 0,\n            \"otp_status\": 0,\n            \"is_active\": 0,\n            \"is_deleted\": 0,\n            \"created_time\": \"2018-03-29T05:45:48.299Z\",\n            \"modified_time\": \"2018-03-29T05:45:48.299Z\"\n        }\n    }\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Failed",
          "content": "HTTP/1.1 400 Failed\n   {\n       \"status\": 400,\n       \"api_name\": \"user_login\",\n       \"message\": \"Login credentials are invalid.\",\n       \"data\": {}\n   }",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "api/controllers/user.js",
    "groupTitle": "User",
    "name": "PostUser_login"
  },
  {
    "type": "post",
    "url": "/user_logout",
    "title": "User Logout",
    "group": "User",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "user_id",
            "description": "<p>User Id</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "device_token",
            "description": "<p>Device Token</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "device_type",
            "description": "<p>Device Type : android / ios</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success",
          "content": "HTTP/1.1 200 OK\n{\n       \"status\": 200,\n       \"api_name\": \"user_logout\",\n       \"message\": \"You have logout successfully.\",\n       \"data\": {}\n   }",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Failed",
          "content": "HTTP/1.1 400 Failed\n   {\n       \"status\": 400,\n       \"api_name\": \"user_logout\",\n       \"message\": \"Some request parameters are missing.\",\n       \"data\": {}\n   }",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "api/controllers/user.js",
    "groupTitle": "User",
    "name": "PostUser_logout"
  },
  {
    "type": "post",
    "url": "/user_register",
    "title": "User Register",
    "group": "User",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "first_name",
            "description": "<p>last_name</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "last_name",
            "description": "<p>last_name</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "email",
            "description": "<p>Email</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "address",
            "description": "<p>address</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "password",
            "description": "<p>Password</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "city",
            "description": "<p>city</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "state",
            "description": "<p>state</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "country",
            "description": "<p>country</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "device_token",
            "description": "<p>Device Token</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "device_type",
            "description": "<p>Device Type : android / ios</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "zip_code",
            "description": "<p>zip_code</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success",
          "content": "   HTTP/1.1 200 OK\n   {\n\t\t\t\"status\": 200,\n\t\t\t\"api_name\": \"user_register\",\n\t\t\t\"message\": \"You have registered successfully.\",\n\t\t\t\"data\": {\n\t\t\t\t\"token\": \"eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHBpcmVzIjoxNTIyNDc2NDU0MzMxLCJ1c2VySUQiOiI1YWJkZDQyNjAyYWNlYTM2Mjk3OGVkODcifQ.cD-UKnY5O96NbVtRWuTRSN4YR6mIw4Zz6EeSuclAr74\",\n\t\t\t\t\"expires\": 1522476454331,\n\t\t\t\t\"user\": {\n\t\t\t\t\t\"fullname\": \"Rahul soni\",\n\t\t\t\t\t\"user_role\": 2,\n\t\t\t\t\t\"email\": \"rahul.soni@arthonsys.com\",\n\t\t\t\t\t\"alternate_email\": \"\",\n\t\t\t\t\t\"phone\": \"\",\n\t\t\t\t\t\"phone_1\": \"\",\n\t\t\t\t\t\"phone_2\": \"\",\n\t\t\t\t\t\"address\": \"chanpol\",\n\t\t\t\t\t\"address_1\": \"\",\n\t\t\t\t\t\"address_2\": \"\",\n\t\t\t\t\t\"city\": \"jaipur\",\n\t\t\t\t\t\"state\": \"rajasthan\",\n\t\t\t\t\t\"zip_code\": \"302012\",\n\t\t\t\t\t\"country\": \"india\",\n\t\t\t\t\t\"latitude\": 26.9243316,\n\t\t\t\t\t\"longitude\": 75.8123829,\n\t\t\t\t\t\"password\": \"777e11fa2c71\",\n\t\t\t\t\t\"user_image\": \"http://35.168.99.29:3001/uploads/default/default_user.jpg\",\n\t\t\t\t\t\"facebook_login_id\": \"\",\n\t\t\t\t\t\"google_login_id\": \"\",\n\t\t\t\t\t\"social_login_data_status\": 0,\n\t\t\t\t\t\"otp_status\": 0,\n\t\t\t\t\t\"is_active\": 0,\n\t\t\t\t\t\"is_deleted\": 0,\n\t\t\t\t\t\"created_time\": \"2018-03-30T06:07:34.322Z\",\n\t\t\t\t\t\"modified_time\": \"2018-03-30T06:07:34.322Z\",\n\t\t\t\t\t\"_id\": \"5abdd42602acea362978ed87\"\n\t\t\t\t}\n\t\t\t}\n\t\t}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Failed",
          "content": "HTTP/1.1 400 Failed\n   {\n       \"status\": 400,\n       \"api_name\": \"user_register\",\n       \"message\": \"Email is already exist.\",\n       \"data\": {}\n   }",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "api/controllers/user.js",
    "groupTitle": "User",
    "name": "PostUser_register"
  },
  {
    "type": "post",
    "url": "/verify_otp",
    "title": "Verify OTP",
    "group": "User",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "email",
            "description": "<p>Email</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "otp_number",
            "description": "<p>OTP Number</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success",
          "content": "HTTP/1.1 200 OK\n{\n       \"status\": 200,\n       \"api_name\": \"verify_otp\",\n       \"message\": \"OTP matched successfully.\",\n       \"data\": {\n           \"user_id\": \"5ab9f851dd88353ccc65cbf2\",\n           \"email\": \"rajeev@gmail.com\"\n       }\n   }",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Failed",
          "content": "HTTP/1.1 400 Failed\n   {\n       \"status\": 400,\n       \"api_name\": \"verify_otp\",\n       \"message\": \"OTP is not exist.\",\n       \"data\": {}\n   }",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "api/controllers/user.js",
    "groupTitle": "User",
    "name": "PostVerify_otp"
  }
] });
