"use strict";
var _      = require('lodash'),
    jwt    = require('jwt-simple'),
    fs     = require('fs'),
	nodemailer = require("nodemailer"),
    config = require('../../config'),
    {user} = require('../models'),
    {crypto} = require('../helpers'),
    {sendmail} = require('../helpers');

var exportFuns = {},
    web = {};

// Display all users
web.all_user=(req,res)=>{
	if(typeof req.session.user_data == "undefined" || req.session.user_data === true)
	{
	    if(typeof req.session.alert_data != "undefined" || req.session.alert_data === true)
	    {
	        res.locals.flashmessages = req.session.alert_data;
	        req.session.alert_data = null;
	    }
		res.redirect('/admin');
	}else
	{
		user.all_user().then(function(user_result) {

			res.render('admin/users/user_list',{"user_data":req.session.user_data,'users':user_result});

		});
   	}
}

// Delete user
web.delete=(req,res)=>{
	if(typeof req.session.user_data == "undefined" || req.session.user_data === true)
	{
	    if(typeof req.session.alert_data != "undefined" || req.session.alert_data === true)
	    {
	        res.locals.flashmessages = req.session.alert_data;
	        req.session.alert_data = null;
	    }
		res.redirect('/admin');
	}else
	{	
		var user_id=req.params.user_id;
		user.delete_user(user_id).then(function(delete_result) {
			res.redirect('/admin/user');

		});
   	}
}
// Edit user
web.edit=(req,res)=>{
	if(typeof req.session.user_data == "undefined" || req.session.user_data === true)
	{
	    if(typeof req.session.alert_data != "undefined" || req.session.alert_data === true)
	    {
	        res.locals.flashmessages = req.session.alert_data;
	        req.session.alert_data = null;
	    }
		res.redirect('/admin');
	}else
	{	
		var user_id=req.params.user_id;
		user.get_user_by_user_id(user_id).then(function(user_details){
			res.render('admin/users/edit_user',{"user_data":req.session.user_data,'user':user_details});
		})
   	}
}

/***************************Update profile***********************************/

web.update_profile = (req, res)=>{
	if(typeof req.session.user_data == "undefined" || req.session.user_data === true){
		res.redirect('/admin');
	}else
	{
		req.session.alert_data = { alert_type: "success", alert_msg: "Successfully Updated." };
		user.update_profile(req.body,req.files).then(function(update_result){
			console.log(update_result);
			if(update_result==1)
			{
				req.session.flash_msg={"msg":"Profile updated Successfully","type":"success"};
				user.get_user_by_user_id(req.session.user_data._id).then(function(result){
				res.redirect('/admin/user');
				});
			}else
			{
				if(update_result==2){
					req.session.flash_msg={"msg":"invaild address","type":"danger"};
				
				}else{
						req.session.flash_msg={"msg":"Profile not updated","type":"danger"};
				}
				user.get_user_by_user_id(req.session.user_data._id).then(function(result){
				res.redirect('/admin/user');
				});
			}
		});
	}
};

/***************************End Update profile**************************** */

//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

exportFuns.web = web;
module.exports = exportFuns;
